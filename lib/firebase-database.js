// Get the configured firebase database
// Require like `const database = require('../lib/firebase-database')()`

const admin = require('firebase-admin')

function database () {
  if (!admin.apps.length) {
    admin.initializeApp({
      databaseURL: process.env.FIREBASE_DATABASE_URL,
      credential: admin.credential.cert({
        projectId: process.env.FIREBASE_PROJECT_ID,
        clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
        privateKey: process.env.FIREBASE_PRIVATE_KEY
      })
    })
  }

  return admin.database
}

module.exports = database
