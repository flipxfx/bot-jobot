// Replace fancy quotes etc from Word to regular keyboard chars
function replaceWordChars (text) {
  return text
    .replace(/[\u2018|\u2019|\u201A]/g, "'")
    .replace(/[\u201C|\u201D|\u201E]/g, '"')
    .replace(/\u2026/g, '...')
    .replace(/[\u2013|\u2014]/g, '-')
    .replace(/\u02C6/g, '^')
    .replace(/\u2039/g, '')
    .replace(/[\u02DC|\u00A0]/g, ' ')
}

module.exports = {
  replaceWordChars
}
