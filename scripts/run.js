// Description:
//   Run some Javascript
//
// Configuration:
//   None
//
// Commands:
//   hubot run X - displays the result of Javascript X (newlines ok)
//   hubot run ```X``` - displays the result of Javascript X (easier for newlines)
//
// Author:
//   flippidippi

const safeEval = require('safe-eval')
const { replaceWordChars } = require('../lib/helpers')

function run (robot) {
  // Runs the code given in RunKit and returns result
  robot.respond(/run(?: |\n|\r| ```)([^]+)$/i, msg => {
    const code = replaceWordChars(msg.match[1].replace(/^```/, '').replace(/```$/, ''))
    const evaluated = safeEval(code)

    msg.send(`:runner:: ${evaluated}`)
  })
}

module.exports = run
