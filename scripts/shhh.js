// Description:
//   Shhh
//
// Configuration:
//   HUBOT_SLACK_TOKEN
//
// Commands:
//   None
//
// Author:
//   flippidippi

const { WebClient } = require('@slack/web-api')

function shhh (robot) {
  const web = new WebClient(process.env.HUBOT_SLACK_TOKEN)

  // Send message to specific room by name (#roomname) or id (C03P5DJEM)
  robot.messageRoom = async (roomNameOrId, txt) => {
    const channels = (await web.channels.list()).channels

    for (const channel of channels) {
      if (channel.id === roomNameOrId || channel.name === roomNameOrId.slice(1)) {
        robot.send({ room: channel.id }, txt)
        return
      }
    }
  }

  // Shhh
  robot.respond(/shhh (?:(#\w+) )?(.+)$/i, async msg => {
    const room = msg.match[1] != null ? msg.match[1] : '#general'

    robot.messageRoom(room, msg.match[2])
  })
}

module.exports = shhh
