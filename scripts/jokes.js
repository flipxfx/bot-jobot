// Description:
//   Deliver some jokes
//
// Configuration:
//   HUBOT_SLACK_TOKEN
//
// Commands:
//   hubot joke pun - displays random pun
//   hubot joke chuck - display random Chuck Norris joke
//
// Author:
//   flippidippi

const he = require('he')

function jokes (robot) {
  // Random pun
  robot.respond(/joke pun$/i, msg => {
    msg.http('https://www.punoftheday.com/cgi-bin/arandompun.pl').get()((err, response, body) => {
      if (!err) {
        const match = body.match(/^document\.write\('&quot;(.*)&quot;<br \/>'\)/)

        msg.send(`:badjokeeel: ${he.decode(match[1])}`)
      }
    })
  })

  // Random Chuck Norris joke
  robot.respond(/joke chuck$/i, msg => {
    msg.http('https://api.chucknorris.io/jokes/random').get()((err, response, body) => {
      if (!err) {
        msg.send(`:chucknorris: ${(JSON.parse(body).value)}`)
      }
    })
  })
}

module.exports = jokes
