// Description:
//   Track lunch picks
//
// Configuration:
//   FIREBASE_PROJECT_ID
//   FIREBASE_CLIENT_EMAIL
//   FIREBASE_PRIVATE_KEY
//   FIREBASE_DATABASE_URL
//
// Commands:
//   hubot lunch - responds with the lunch schedule
//   hubot lunch join - join the lunch club, resets user info and pick
//   hubot lunch pick - displays your lunch pick
//   hubot lunch picks - displays all current lunch picks
//   hubot lunch pick X - sets your lunch pick
//   hubot lunch leave - leave the lunch club
//   hubot lunch site - links to the lunch site
//   hubot lunch skip - sets current picker to next in list
//   hubot lunch unskip - sets current picker to previous in list
//
// Author:
//   flippidippi

const database = require('../lib/firebase-database')()
const db = database()
const momentBase = require('moment')
const momentRange = require('moment-range')
const moment = momentRange.extendMoment(momentBase)

// Gets the lunch schedule
async function getSchedule (callback) {
  const lunch = (await db.ref('lunch').once('value')).val()

  // Build sorted user list
  const users = Object.values(lunch.users).sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))

  // Starting pick date for the first user
  const baseDate = moment(lunch.base.date)

  // Calculate current date
  const currentDate = moment().startOf('day')
  if (currentDate.day() > baseDate.day()) {
    currentDate.day(baseDate.day() + 7)
  } else {
    currentDate.day(baseDate.day())
  }

  // Calculate current user
  const weekDiff = moment.range(baseDate, currentDate).diff('weeks')
  const currentUser = weekDiff % users.length

  // Build user list
  let list = ''
  for (const [index, user] of users.entries()) {
    if (index !== currentUser) {
      list += `${user.name}`
    } else {
      list += `*${user.name} - ${(user.pick ? user.pick : 'TBD')}*`
    }
    if (index !== users.length - 1) {
      list += ', '
    }
  }

  return `*${currentDate.format('dddd, MMMM D')}* | ${list}`
}

function lunch (robot) {
  // Lunch schedule
  robot.respond(/lunch$/i, async msg => {
    const schedule = await getSchedule()

    msg.send(schedule)
  })

  // Join lunch club
  robot.respond(/lunch join$/i, msg => {
    const user = robot.brain.data.users[msg.message.user.id]

    db.ref(`lunch/users/${user.id}`).set({ name: user.slack.profile.first_name, pick: '' })
    msg.send(`@${user.name} joined the lunch club :thumbsup: :tada:`)
  })

  // Display lunch pick
  robot.respond(/lunch pick$/i, async msg => {
    const user = robot.brain.data.users[msg.message.user.id]
    const userInfo = (await db.ref(`lunch/users/${user.id}`).once('value')).val()

    if (userInfo) {
      const pick = userInfo.pick

      if (pick) {
        msg.send(`@${user.name}'s lunch pick is *${(userInfo.pick)}*`)
      } else {
        msg.send(`@${user.name} has no lunch pick :slightly_frowning_face:`)
      }
    } else {
      msg.send(`@${user.name} is not in the lunch club :thumbsdown:`)
    }
  })

  // Display lunch picks
  robot.respond(/lunch picks$/i, async msg => {
    const users = (await db.ref('lunch/users').once('value')).val()

    // Build sorted user list
    const usersSorted = Object.values(users).sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))

    let list = ''
    for (const user of usersSorted) {
      list += `- ${user.name} - ${(user.pick ? user.pick : 'TBD')}\n`
    }

    msg.send(list)
  })

  // Set lunch pick
  robot.respond(/lunch pick (.+)$/i, async msg => {
    const user = robot.brain.data.users[msg.message.user.id]
    const userInfo = (await db.ref(`lunch/users/${user.id}`).once('value')).val()

    if (userInfo) {
      if (msg.match[1].length <= 40) {
        db.ref(`lunch/users/${user.id}`).update({ pick: msg.match[1] })
        msg.send(`@${user.name} set lunch pick to *${msg.match[1]}*`)
      } else {
        msg.send(`@${user.name} tried to set the lunch pick some obnoxiously long name :disapproval:`)
      }
    } else {
      msg.send(`@${user.name} is not in the lunch club :thumbsdown:`)
    }
  })

  // Leave lunch club
  robot.respond(/lunch leave$/i, msg => {
    const user = robot.brain.data.users[msg.message.user.id]

    db.ref(`lunch/users/${user.id}`).set(null)
    msg.send(`@${user.name} left the lunch club :thumbsdown:`)
  })

  // Skip lunch for current turn
  robot.respond(/lunch skip/i, async msg => {
    const lunch = (await db.ref('lunch').once('value')).val()

    const baseDate = moment(lunch.base.date)
    baseDate.add(-7, 'days')

    db.ref('lunch/base').update({ date: baseDate.format('YYYY-MM-DD') })

    const schedule = await getSchedule()
    msg.send('Lunch skipped')
    msg.send(schedule)
  })

  // Unskip lunch for current turn
  robot.respond(/lunch unskip/i, async msg => {
    const lunch = (await db.ref('lunch').once('value')).val()

    const baseDate = moment(lunch.base.date)
    baseDate.add(7, 'days')

    db.ref('lunch/base').update({ date: baseDate.format('YYYY-MM-DD') })

    const schedule = await getSchedule()
    msg.send('Lunch unskipped')
    msg.send(schedule)
  })

  // Open the lunch site
  robot.respond(/lunch site$/i, msg => {
    msg.send('https://flippidippi.com/lunch')
  })
}

module.exports = lunch
