#!/usr/bin/env node

const cp = require('child_process')

// Load env file for dev
if (process.env.NODE_ENV === 'development') {
  require('dotenv').config()
}

cp.spawn('./jobot', process.argv.slice(2), { stdio: 'inherit' })
